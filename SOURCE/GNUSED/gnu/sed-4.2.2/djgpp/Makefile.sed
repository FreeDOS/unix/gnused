# Remove LINGUAS from dependecy list in ./po/Makefile
/^Makefile[ 	]*:/ s/LINGUAS$/#&/

Remove tst-pcre$(EXEEXT), tst-boost$(EXEEXT) and tst-rxspencer$(EXEEXT) in ./testsuite/Makefile
/check_PROGRAMS[ 	]*=/,/am__append_1[ 	]*=/ {
  /tst-pcre\$(EXEEXT)/d
  /tst-boost\$(EXEEXT)/d
  s/tst-rxspencer\$(EXEEXT)//
}
/^version\.good[ 	]*:/,/^$/ {
  /^$/ i\
	dtou $(subdir)/$@
}
