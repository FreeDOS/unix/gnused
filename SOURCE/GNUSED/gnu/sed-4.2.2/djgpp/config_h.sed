# Sed script to add DJGPP specific issues to config.h.


$ a\
\
\
/* DGJPP-specific definitions */\
\
#ifdef __DJGPP__\
\
# include <sys/version.h>  /*  gcc no longer includes this by default.  */\
#endif

